import uvm_pkg::*;
`include "uvm_macros.svh"
class req_ack_env extends uvm_env;
  `uvm_component_utils (req_ack_env)

  function new(string name = "req_ack_env", uvm_component parent);
    super.new(name, parent);
  endfunction: new

  req_ack_agent agnt0;
  req_ack_agent agnt1;
  req_ack_agent agnt2;
  req_ack_agent agnt3;
  req_ack_scoreboard scbd0;
  req_ack_virtual_seqr virt_seqr0;
  

  virtual function void build_phase (uvm_phase phase);
    super.build_phase(phase);
    agnt0 = req_ack_agent::type_id::create("req_ack_agent0",this);
    agnt1 = req_ack_agent::type_id::create("req_ack_agent1",this);
    agnt2 = req_ack_agent::type_id::create("req_ack_agent2",this);
    agnt3 = req_ack_agent::type_id::create("req_ack_agent3",this);
    scbd0 = req_ack_scoreboard::type_id::create("req_ack_scoreboard",this);
    virt_seqr0 = req_ack_virtual_seqr::type_id::create("req_ack_virtual_seqr",this);
  endfunction
  
  virtual function void connect_phase(uvm_phase phase);
    agnt0.mon0.mon_analysis_port.connect(scbd0.ap_imp);
    agnt1.mon0.mon_analysis_port.connect(scbd0.ap_imp);
    agnt2.mon0.mon_analysis_port.connect(scbd0.ap_imp);
    agnt3.mon0.mon_analysis_port.connect(scbd0.ap_imp);

    virt_seqr0.req_ack_seqr_0 = agnt0.seqr0;
    virt_seqr0.req_ack_seqr_1 = agnt1.seqr0;       
    virt_seqr0.req_ack_seqr_2 = agnt2.seqr0;
    virt_seqr0.req_ack_seqr_3 = agnt3.seqr0;

  endfunction

endclass : req_ack_env
