import uvm_pkg::*;
`include "uvm_macros.svh"
class req_ack_item extends uvm_sequence_item;
 
  rand bit wr_n;
  rand reg[7:0] wdata;
  rand reg[7:0] rdata;


  typedef enum {b2b,short,med,long} delay_t;

  rand delay_t trans_delay_type;
  rand int trans_delay;

  constraint c_trans_delay{
    (trans_delay_type == b2b)   -> trans_delay == 0;
    (trans_delay_type == short) -> trans_delay inside{[1:10]};
    (trans_delay_type == med)   -> trans_delay inside{[10:100]};
    (trans_delay_type == long)  -> trans_delay inside{[100:1000]};
  }
//UVM macros
  `uvm_object_utils_begin(req_ack_item)
    `uvm_field_int  (wr_n,                        UVM_DEFAULT)
    `uvm_field_int  (wdata,                       UVM_DEFAULT)
    `uvm_field_int  (rdata,                       UVM_DEFAULT)
    `uvm_field_enum (delay_t, trans_delay_type,   UVM_DEFAULT)
    `uvm_field_int  (trans_delay,                 UVM_DEFAULT)
  `uvm_object_utils_end

  function new (string name ="req_ack_item"); 
    super.new(name);
  endfunction
endclass : req_ack_item