import uvm_pkg::*;
`include "uvm_macros.svh"
//base sequence
class req_ack_base_seq extends uvm_sequence #(req_ack_item);
  `uvm_object_utils(req_ack_base_seq)

  function new(string name = "req_ack_base_seq");
    super.new(name);
  endfunction: new

  virtual task pre_body();
    `uvm_info("Base sequence",$sformatf("Pre_body raising objection"),UVM_MEDIUM)
        if(starting_phase!=null)
          starting_phase.raise_objection(this);
  
  endtask

  virtual task post_body();
  `uvm_info("Base sequence", $sformatf("Post_body dropping objection"),UVM_MEDIUM)
    if(starting_phase!=null)
      starting_phase.drop_objection(this);
  endtask

endclass: req_ack_base_seq

//client 0 sequence
class req_ack_seq extends req_ack_base_seq;
  `uvm_object_utils(req_ack_seq)

  rand int nr_trans;

  constraint c_trans {nr_trans<100;}

  function new(string name = "req_ack_seq");
    super.new(name);
  endfunction: new

  virtual task body();
   
    req_ack_item item;
    //nr_trans = $urandom_range(20,1);
    item = req_ack_item::type_id::create("item");
    repeat(nr_trans)begin
      start_item(item);
      if(!item.randomize() with {trans_delay inside {[1:10]};})
        `uvm_error(get_type_name,"Randomization error!")
      finish_item(item);
    end
  endtask
endclass: req_ack_seq

//virtual sequence
class req_ack_virt_seq extends uvm_sequence;
  `uvm_object_utils(req_ack_virt_seq);
  `uvm_declare_p_sequencer(req_ack_virtual_seqr);

  function new(string name = "req_ack_virt_seq");
    super.new(name);
  endfunction: new
  //req_ack_seq req_ack_real_seq;
  req_ack_seq req_ack_seq_0;
  req_ack_seq req_ack_seq_1;
  req_ack_seq req_ack_seq_2;
  req_ack_seq req_ack_seq_3;

  task pre_body();
    req_ack_seq_0 = req_ack_seq::type_id::create("req_ack_seq_0");
    req_ack_seq_1 = req_ack_seq::type_id::create("req_ack_seq_1");
    req_ack_seq_2 = req_ack_seq::type_id::create("req_ack_seq_2");
    req_ack_seq_3 = req_ack_seq::type_id::create("req_ack_seq_3");
    //req_ack_seq_0 = req_ack_seq::type_id::create("req_ack_real_seq");
  endtask

  task body();
    req_ack_seq_0.randomize() with {nr_trans inside {[1:10]};};
    req_ack_seq_1.randomize() with {nr_trans inside {[5:10]};};
    req_ack_seq_2.randomize() with {nr_trans inside {[1:20]};};
    req_ack_seq_3.randomize() with {nr_trans==3;};
    fork
      req_ack_seq_0.start (p_sequencer.req_ack_seqr_0);
      req_ack_seq_1.start (p_sequencer.req_ack_seqr_1);
      req_ack_seq_2.start (p_sequencer.req_ack_seqr_2);
      req_ack_seq_3.start (p_sequencer.req_ack_seqr_3);
    join
    disable fork;
  endtask
endclass: req_ack_virt_seq 