import uvm_pkg::*;
`include "uvm_macros.svh"

class req_ack_monitor extends uvm_monitor;

  req_ack_item data;

  `uvm_component_utils(req_ack_monitor)
  
  function new(string name = "req_ack_monitor", uvm_component parent = null);
    super.new(name, parent);
  endfunction

  virtual req_ack_if vif;

  uvm_analysis_port #(req_ack_item) mon_analysis_port;

  //build the uvm monitor
  virtual function void build_phase(uvm_phase phase);
    super.build_phase(phase);

    mon_analysis_port = new("mon_analysis_port", this);

    //get the virtual if handle from the cofiguration DB
    if(!uvm_config_db #(virtual req_ack_if)::get(this,"","req_ack_if",vif)) begin
      `uvm_error(get_type_name(),"DUT interface not found")
    end
  endfunction

  virtual task run_phase(uvm_phase phase);
    //super.run_phase(phase);
    data = req_ack_item::type_id::create("data");

    forever begin
      @(posedge vif.ack); 
      if(!vif.wr_n)           //daca wr = scriere
        data.wdata = vif.wdata;     //in data se vor stoca datele de scriere
      else
        data.rdata = vif.rdata;     //in data se vor stoca datele de citire
      
      mon_analysis_port.write(data);
    end
  endtask;

endclass : req_ack_monitor