import uvm_pkg::*;
`include "uvm_macros.svh"
class req_ack_virtual_seqr extends uvm_sequencer #(req_ack_item);
  `uvm_component_utils(req_ack_virtual_seqr)

  function new(string name = "req_ack_virtual_seqr", uvm_component parent);
    super.new(name, parent);
  endfunction

  //declare handles to other seqr here
  uvm_sequencer#(req_ack_item) req_ack_seqr_0;
  uvm_sequencer#(req_ack_item) req_ack_seqr_1;
  uvm_sequencer#(req_ack_item) req_ack_seqr_2;
  uvm_sequencer#(req_ack_item) req_ack_seqr_3;

  function void build_phase(uvm_phase phase);
    super.build_phase(phase);
  endfunction

endclass : req_ack_virtual_seqr