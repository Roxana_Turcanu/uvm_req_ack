import uvm_pkg::*;
`include "uvm_macros.svh"
class req_ack_test extends uvm_test;
  `uvm_component_utils(req_ack_test)

  function new(string name = "req_ack_test",uvm_component parent = null);
    super.new(name,parent);
  endfunction

  req_ack_env  top_env;
  virtual req_ack_if  vif_cl0;
  virtual req_ack_if  vif_cl1;
  virtual req_ack_if  vif_cl2;
  virtual req_ack_if  vif_cl3;
  //req_ack_cfg  cfg0;

  virtual function void build_phase (uvm_phase phase);
    super.build_phase(phase);
    //create env
    top_env = req_ack_env::type_id::create("top_env", this);

    //get interface from top
    if(!uvm_config_db #(virtual req_ack_if)::get(this, "", "req_ack_if0", vif_cl0)) begin
      `uvm_fatal(get_type_name(),"Didn't get handle to virtual if. req_ack_if0")
    end

    if(!uvm_config_db #(virtual req_ack_if)::get(this, "", "req_ack_if1", vif_cl1)) begin
      `uvm_fatal(get_type_name(),"Didn't get handle to virtual if. req_ack_if1")
    end

    if(!uvm_config_db #(virtual req_ack_if)::get(this, "", "req_ack_if2", vif_cl2)) begin
      `uvm_fatal(get_type_name(),"Didn't get handle to virtual if. req_ack_if2")
    end

    if(!uvm_config_db #(virtual req_ack_if)::get(this, "", "req_ack_if3", vif_cl3)) begin
      `uvm_fatal(get_type_name(),"Didn't get handle to virtual if. req_ack_if3")
    end

    uvm_config_db #(virtual req_ack_if) :: set(this, "top_env.req_ack_agent0*","req_ack_if",vif_cl0);
    uvm_config_db #(virtual req_ack_if) :: set(this, "top_env.req_ack_agent1*","req_ack_if",vif_cl1);
    uvm_config_db #(virtual req_ack_if) :: set(this, "top_env.req_ack_agent2*","req_ack_if",vif_cl2);
    uvm_config_db #(virtual req_ack_if) :: set(this, "top_env.req_ack_agent3*","req_ack_if",vif_cl3);

  endfunction

  virtual task run_phase(uvm_phase phase);
    //create and instantiate the sequence
    req_ack_virt_seq seq0 = req_ack_virt_seq::type_id::create("seq0");

    //raise objection - else this test will not condume simulation time
    phase.raise_objection(this);
    //start the sequence on a given sequencer
    seq0.start(top_env.virt_seqr0);
    //drop objection - else this test will not finish
    phase.drop_objection(this);
  endtask


endclass:req_ack_test