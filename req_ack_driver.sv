import uvm_pkg::*;
`include "uvm_macros.svh"
class req_ack_driver extends uvm_driver #(req_ack_item);
  `uvm_component_utils(req_ack_driver)

  function new(string name = "req_ack_driver", uvm_component parent = null);
    super.new(name, parent);
  endfunction
  
  //declaration of the virtual interface object
  virtual req_ack_if vif;

  virtual function void build_phase(uvm_phase phase);
    super.build_phase (phase);
    if(!uvm_config_db #(virtual req_ack_if)::get(this, "","req_ack_if",vif)) begin
      `uvm_fatal(get_type_name(),"Didn't get handle to virtual if req_ack_if")
    end
  endfunction
 //uvm_test_top ,

  virtual task run_phase(uvm_phase phase);
    req_ack_item req_item;
    
    forever begin
      seq_item_port.get_next_item(req_item);

      this.vif.req   = 'b0;
      this.vif.wdata = 'b0;
      this.vif.wr_n = 'b0;
      @(posedge vif.clk);
      this.vif.req   = 'b1;
      this.vif.wr_n  = $urandom_range(0,1); //wr read sau write
      if(this.vif.wr_n==0)        //daca wr = scriere
        this.vif.wdata = $random;  //generare date random
      @(posedge vif.ack); 
      @(posedge vif.clk);
      this.vif.req = 'b0;
      this.vif.wdata = 'b0;

      seq_item_port.item_done();
    end
  endtask

endclass: req_ack_driver
