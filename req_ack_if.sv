import uvm_pkg::*;
`include "uvm_macros.svh"
interface req_ack_if#(
parameter WIDTH  = 'd8 //numarul de biti ai datelor citite/scrise
)(
    input clk,
    input rst
);

reg       req;
reg       ack;
reg       wr_n;
//reg       ack_dly;
reg[WIDTH -1:0] rdata; 
reg[WIDTH -1:0] wdata; 

/*
time      current_time;

function void file_write(string message);
  int file;
  
  file = $fopen("test_status.txt", "a");
  $fwrite(file, message);
  $fclose(file);
  endfunction  

always @(posedge clk or posedge rst)
if (rst)
  ack_dly <= 1'b0; else
  ack_dly <= ack;
        
//check all sig. values at reset
property p_init_req;
    @(posedge clk)
        (rst|-> ~req);     
endproperty

a_init_req : assert property (p_init_req) else
        begin
        $display("Assertion a_init_req failed at %0t", current_time);
        //$stop;
        end
//check ack at reset
property p_init_ack;
        @(posedge clk)
            (rst|-> ~ack);     
    endproperty
    
a_init_ack : assert property (p_init_ack) else
        begin
        $display("Assertion a_init_ack failed at %0t", current_time);
        file_write("Assertion a_init_ack failed! \n");
        $stop;
        end
//check wr_n at reset
property p_init_wr_n;
    @(posedge clk)
        (rst|-> ~wr_n);     
endproperty

a_init_wr_n : assert property (p_init_wr_n) else
begin
        $display("Assertion a_init_wr_n failed at %0t", current_time);
        file_write("Assertion a_init_wr_n failed! \n");
        $stop;
end
//check rdata at reset
property p_init_rdata;
        @(posedge clk)
            (rst|-> ~rdata);     
endproperty
    
a_init_rdata : assert property (p_init_rdata) else
begin
        $display("Assertion a_init_rdata failed at %0t", current_time);
        file_write("Assertion a_init_rdata failed! \n");
        $stop;
end
//check wdata at reset
property p_init_wdata;
        @(posedge clk)
            (rst|-> ~wdata);     
endproperty
    
a_init_wdata : assert property (p_init_wdata) else
begin
        $display("Assertion a_init_wdata failed at %0t", current_time);
        file_write("Assertion a_init_wdata failed! \n");
        $stop;
end


//req will be active until ack received
property p_req_check;
    @(posedge clk) disable iff(rst)
         ($fell(req) && $fell(ack) |-> $rose(ack_dly)) ;
endproperty

a_req_check : assert property (p_req_check) else
begin
        $display("Assertion a_req_check failed at %0t", current_time);
        file_write("Assertion a_req_check failed! \n");
        $stop;
end 

//request data write valid while request is active;
property p_req_wr_data_valid;
    @(posedge clk) disable iff(rst)
    (req |-> !$isunknown(wdata)); //sau $unknown(wdata) (wdata != 'bz)
endproperty

a_req_wr_data_valid : assert property (p_req_wr_data_valid) else
begin
        $display("Assertion a_req_wr_data_valid failed at %0t", current_time);
        file_write("Assertion a_req_wr_data_valid failed! \n");
        $stop;
end
//check if ack is received
property p_ack_received;
    @(posedge clk) disable iff(rst)
        (req |=> ##[1:300]ack);     
endproperty
    
a_ack_received  : assert property (p_ack_received ) else
begin
        $display("Assertion a_ack_received failed at %0t", current_time);
        file_write("Assertion a_ack_received failed! \n");
        $stop;
end
//ack active 1 clk
property p_ack_period_check;
    @(posedge clk) disable iff(rst)
        (ack |=> ~ack);     
endproperty
    
a_ack_period_check  : assert property (p_ack_period_check) else
begin
        $display("Assertion ack_period_check failed at %0t", current_time);  
        file_write("Assertion a_ack_period_check failed! \n");         
        $stop;
end
//ack data read valid while ack is active;
property p_ack_rd_data_valid;
    @(posedge clk) disable iff(rst)
    (req |-> !$isunknown(rdata));//$unknown(rdata));  
endproperty

a_ack_rd_data_valid : assert property (p_ack_rd_data_valid) else
begin
        $display("Assertion a_ack_wr_data_valid failed at %0t", current_time);
        file_write("Assertion a_ack_rd_data_valid failed! \n");
        $stop;
end
//ack should not activate without req active
property p_ack_check;
    @(posedge clk) disable iff(rst)
       (ack |-> req);     
endproperty
    
a_ack_check : assert property (p_ack_check) else
begin
        $display("Assertion a_ack_check failed at %0t", current_time);
        file_write("Assertion a_ack_check failed! \n");
        $stop;
end
//data read shall not be modified while reading (read = wr_n)     
property p_data_rd_check;
    @(posedge clk) disable iff(rst)
        ((ack && wr_n && !$rose(ack) && !$fell(req)) |-> $stable(rdata)); 
endproperty
    
a_data_rd_check : assert property (p_data_rd_check) else
begin
        $display("Assertion a_data_rd_check failed at %0t", current_time);
        file_write("Assertion a_data_rd_check  failed! \n");
        $stop;
end
//data write shall not be modified while writing (write = ~wr_n)
property p_data_wr_check;
    @(posedge clk) disable iff(rst)
       // (req && ~wr_n && $past(req)) |-> $stable(wdata);  
        ((req && ~wr_n && !$rose(req) && !$fell(ack)) |-> $stable(wdata));   
endproperty
    
a_data_wr_check : assert property (p_data_wr_check) else
begin
        $display("Assertion a_data_wr_check failed at %0t", current_time);
        file_write("Assertion a_data_wr_check  failed! \n");
       // $stop;
end

//check wr_n value at req
property p_wr_rd_check;
    @(posedge clk) disable iff(rst)
        ( req |-> $isunknown(wr_n)==0);    //(wr_n != 'bz)
endproperty
    
a_wr_rd_check : assert property (p_wr_rd_check) else
begin
        $display("Assertion wr_rd_check: wr_n has the value:%0h", wr_n);
        file_write("Assertion a_wr_rd_check failed! \n");
        $stop;
end

//Coverage:
property p_rd_cl;
  @(posedge clk) disable iff(rst)
  (req && wr_n);
endproperty

property p_wr_cl;
  @(posedge clk) disable iff(rst)
  (req && (!wr_n));
endproperty

property p_b2b_req_cl;
  @(posedge clk) disable iff(rst)
  ($rose(ack) && $past(req) && req);
endproperty

property p_req_ack_tmg;
  @(posedge clk) disable iff(rst)
  (($rose(req) || $fell(ack)) ##[1:100] ack);
endproperty

property p_ack_tmg;
  @(posedge clk) disable iff(rst)
  ($fell(ack) ##[1:100] ack);
endproperty

property p_req_tmg;
  @(posedge clk) disable iff(rst)
  ((($fell(ack) && req) || $rose(req)) ##[1:100] ack);
endproperty

cp_rd_cl      :cover property(p_rd_cl);
cp_wr_cl      :cover property(p_wr_cl);
cp_b2b_req_cl :cover property(p_b2b_req_cl);
cp_ack_tmg    :cover property(p_ack_tmg);
cp_req_tmg    :cover property(p_req_tmg);
cp_req_ack_tmg:cover property(p_req_ack_tmg);


// TODO : Add assertions and coverage here
// http://www.asic-world.com/systemverilog/assertions.html
// http://www.asic-world.com/systemverilog/coverage.html
*/
endinterface : req_ack_if