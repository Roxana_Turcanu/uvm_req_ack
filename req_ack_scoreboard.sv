import uvm_pkg::*;
`include "uvm_macros.svh"
class req_ack_scoreboard extends uvm_scoreboard;
  `uvm_component_utils(req_ack_scoreboard)

  function new(string name = "req_ack_scoreboard", uvm_component parent);
    super.new(name, parent);
  endfunction

  //declaration of TLM analysis port to receive data obj 
  uvm_analysis_imp #(req_ack_item, req_ack_scoreboard) ap_imp;
  
  //instantiate the analysis port
  function void build_phase(uvm_phase phase);
    ap_imp = new("ap_imp",this);
  endfunction
  
  //define action to be taken when a packet is received via the declared analysis port
  virtual function void write(req_ack_item pkt);
    //what should be done with the data after packet received comes here
    `uvm_info("write",$sformatf("Data recdeived: %0h", pkt), UVM_MEDIUM)
  endfunction
  
  endclass: req_ack_scoreboard
  