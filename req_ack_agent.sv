import uvm_pkg::*;
`include "uvm_macros.svh"
class req_ack_agent extends uvm_agent;
//makes this agent more re-usable
  `uvm_component_utils(req_ack_agent)

function new(string name = "req_ack_agent", uvm_component parent);
  super.new(name, parent);
endfunction: new
    
//instances of all agent components
req_ack_driver  drv0;
req_ack_monitor mon0;
//req_ack_sequencer seqr0;
uvm_sequencer#(req_ack_item) seqr0;

virtual function void build_phase (uvm_phase phase);
  if(get_is_active()) begin
      seqr0 = uvm_sequencer#(req_ack_item)::type_id::create("seqr0",this);
      $display("TEst display");
      //seqr0 = req_ack_sequencer::type_id::create("seqr0",this);
      drv0 = req_ack_driver::type_id::create("drv0",this);
  end
//both active and passive agents need a monitor:
  mon0 = req_ack_monitor::type_id::create("mon0",this);
endfunction

//connect agent comonents together
virtual function void connect_phase(uvm_phase phase);
  //connect driver to sequencer 
  if(get_is_active())
    drv0.seq_item_port.connect(seqr0.seq_item_export);
endfunction

endclass: req_ack_agent
    