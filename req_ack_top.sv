module req_ack_top;
  import uvm_pkg::*;
  `include "uvm_macros.svh"

  bit clk;
  always #10 clk <= ~clk; //geneate clk
  bit rst;  
  initial begin   //generate reset
    rst = 1;
    #10;
    rst = 0;
  end

  //interfaces
  req_ack_if i_if_cl0 (.clk(clk), .rst(rst));
  req_ack_if i_if_cl1 (.clk(clk), .rst(rst));
  req_ack_if i_if_cl2 (.clk(clk), .rst(rst));
  req_ack_if i_if_cl3 (.clk(clk), .rst(rst));

  initial begin
    uvm_config_db #(virtual req_ack_if)::set(null,"uvm_test_top","req_ack_if0", i_if_cl0);
    uvm_config_db #(virtual req_ack_if)::set(null,"uvm_test_top","req_ack_if1", i_if_cl1);
    uvm_config_db #(virtual req_ack_if)::set(null,"uvm_test_top","req_ack_if2", i_if_cl2);
    uvm_config_db #(virtual req_ack_if)::set(null,"uvm_test_top","req_ack_if3", i_if_cl3);    
    `uvm_info("TEST",$sformatf("Test is running"), UVM_NONE)
    run_test("req_ack_test");
  end

//generate ack cl0
  initial begin
    i_if_cl0.ack = 'b0;
    i_if_cl0.rdata = 'b0;
    i_if_cl0.wr_n = 'b0;
    forever begin
      wait(i_if_cl0.req);
      repeat ($urandom_range(0,10)) @(posedge i_if_cl0.clk); //delay intre 1-10 tacte
      if(i_if_cl0.wr_n)        //daca wr = citire
        i_if_cl0.rdata = $random;  //generare date random
      i_if_cl0.ack = 'b1;      //se ridica ack
      @(posedge i_if_cl0.clk);   
      i_if_cl0.ack = 'b0; 
      i_if_cl0.rdata = 'b0;
    end
  end

  //cl1
  initial begin
    i_if_cl1.ack = 'b0;
    i_if_cl1.rdata = 'b0;
    i_if_cl1.wr_n = 'b0;
    forever begin
      wait(i_if_cl1.req);
      repeat ($urandom_range(0,10)) @(posedge i_if_cl1 .clk); //delay intre 1-10 tacte
      if(i_if_cl1.wr_n)        //daca wr = citire
        i_if_cl1.rdata = $random;  //generare date random
      i_if_cl1.ack = 'b1;      //se ridica ack
      @(posedge i_if_cl1.clk);   
      i_if_cl1.ack = 'b0; 
      i_if_cl1.rdata = 'b0;
    end
  end

  //cl2
  initial begin
    i_if_cl2.ack = 'b0;
    i_if_cl2.rdata = 'b0;
    i_if_cl2.wr_n = 'b0;
    forever begin
      wait(i_if_cl2.req);
      repeat ($urandom_range(0,10)) @(posedge i_if_cl2.clk); //delay intre 1-10 tacte
      if(i_if_cl2.wr_n)        //daca wr = citire
        i_if_cl2.rdata = $random;  //generare date random
      i_if_cl2.ack = 'b1;      //se ridica ack
      @(posedge i_if_cl2.clk);   
      i_if_cl2.ack = 'b0; 
      i_if_cl2.rdata = 'b0;
    end
  end

  //cl3
  initial begin
    i_if_cl3.ack = 'b0;
    i_if_cl3.rdata = 'b0;
    i_if_cl3.wr_n = 'b0;
    forever begin
      wait(i_if_cl3.req);
      repeat ($urandom_range(0,10)) @(posedge i_if_cl3.clk); //delay intre 1-10 tacte
      if(i_if_cl3.wr_n)        //daca wr = citire
        i_if_cl3.rdata = $random;  //generare date random
      i_if_cl3.ack = 'b1;      //se ridica ack
      @(posedge i_if_cl3.clk);   
      i_if_cl3.ack = 'b0; 
      i_if_cl3.rdata = 'b0;
    end
  end
endmodule