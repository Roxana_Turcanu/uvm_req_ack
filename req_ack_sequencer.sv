import uvm_pkg::*;
`include "uvm_macros.svh"

class req_ack_sequencer extends uvm_sequencer;
    `uvm_component_utils(req_ack_sequencer)

  function new(string name = "req_ack_sequencer", uvm_component parent);
    super.new(name, parent);
  endfunction: new
  
endclass : req_ack_sequencer